<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('halaman.form');
    }

    public function send(Request $request)
    {
       $name1 = $request['FirstName'];
       $name2 = $request['LastName'];

       return view('halaman.welcome', compact('name1', 'name2'));
    }
}
