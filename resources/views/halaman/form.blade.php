@extends('layout.master')

@section('judul')
Halaman Form  
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="get">
@csrf
<p>First name:</p>
    <input type="text" name="FirstName">
<p>Last name:</p>
    <input type="text" name="LastName">
<p>Gender:</p>
    <input type="radio" id="male" name="gender"><label for="male">Male</label>
        <label for="vehicle1"> </label><br>
    <input type="radio" id="Female" name="gender"><label for="Female">Female</label>
        <label for="vehicle1"> </label><br>
    <input type="radio" id="other" name="gender"><label for="other">Other</label>
        <label for="vehicle1"> </label><br>
<p name="nasional">Nationality:</p>
    <select>
        <option>Indonesian</option>
        <option>Amerika</option>
        <option>Inggris</option>
    </select>   
<p>Language Spoken:</p>
    <input type="checkbox" id="vehicle1" name="vehicle1" value="Indonesia">
        <label for="vehicle1"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="vehicle2" name="vehicle2" value="English">
        <label for="vehicle2"> English</label><br>
    <input type="checkbox" id="vehicle3" name="vehicle3" value="Other">
        <label for="vehicle3"> Other</label>
<p>Bio:</p>
        <textarea style="resize:none;width:300px;height:100px;"></textarea>
<br>
    <button type="submit"> Sign Up</button>
</form> 
</form>
@endsection