@extends('layout.master')

@section('judul')
Cast Edit {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$cast->nama}}" name="nama">
        </div>
        @error('nama')
    <div class="alert alert-danger">
            {{ $message }}
    </div>
        @enderror
    
    <div class="form-group">
        <label>Umur</label>
         <input type="text" class="form-control" {{$cast->umur}} name="umur">
         </div>
        @error('umur')
    <div class="alert alert-danger">
            {{ $message }}
    </div>
        @enderror
    
    <div class="form-group">
        <label>Bio</label>
         <textarea name="bio" class="form-control" {{$cast->bio}} name="bio" cols="30" rows="10"></textarea>
    </div>
        @error('bio')
    <div class="alert alert-danger">
            {{ $message }}
    </div>
        @enderror
  
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection